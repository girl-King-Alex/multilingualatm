﻿using System;

namespace MultiLingualATM
{
    public class Account
    {
        int? BVN;
        string password;

        public int? setBvn
        {
            get
            {
                return BVN;
            }

            set
            {
                if (value.ToString().Length > 8 || value.ToString().Length < 8)
                {

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("\nError Alert");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("  Your BVN should be 8- digits, your input is incorrect");
                    Console.ForegroundColor = ConsoleColor.White;

                }
                else if (value.ToString().Length == 8)
                {
                    BVN = value;
                }
            }
        }

        public string setPassword
        {
            get
            {
                return password;
            }
            set
            {
                if (value.Length == 4)
                {
                    password = value;
                }
                else
                {
                    password = "0000";
                    Console.WriteLine("ur pw as been set to 0000");
                }

            }

        }
        public string FullName { get; set; }
        // public string LastName { get; set; }
        public int AccountBalance { get; set; }
        public int CVV { get; set; }
    }
}
