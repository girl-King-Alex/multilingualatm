﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MultiLingualATM
{
    class ATM
    {
        protected static void CheckBalance(string name, string password, SortedDictionary<int, Account> database)
        {
            var result = from Database in database
                         where Database.Value.FullName.Equals(name)
                         where Database.Value.setPassword.Equals(password)
                         select Database.Value.AccountBalance;

            // var result = database.Where(a => a.Value.FullName.Equals(name)).Where(b => b.Value.setPassword.Equals(password)).Select(a => a.Value.AccountBalance);

            foreach (var item in result)
            {
                Console.WriteLine($"Your Account balance is {item}");
            }
        }

        protected static void WithdrawCash(string name, string password, SortedDictionary<int, Account> database)
        {
            int AccountBalance = 0;
            var result = from Database in database
                         where Database.Value.FullName.Equals(name)
                         where Database.Value.setPassword.Equals(password)
                         select (Database.Value.AccountBalance);

            foreach (var item in result)
            {
                AccountBalance = item;
            }

            // var result = database.Where(a => a.Value.FullName.Equals(name)).Where(b => b.Value.setPassword.Equals(password)).Select(a => a.Value.AccountBalance);
            Console.Clear();
            Console.WriteLine($"Your Account balance is {AccountBalance}");
            Console.WriteLine("How much will you like to withdraw");
            int UserReply = Convert.ToInt32(Console.ReadLine());

            if (UserReply < AccountBalance)
            {
                AccountBalance -= UserReply;
                Console.WriteLine("\nIn what denominations will you like your cash to be \n1. 1000 Naira\n2. 500 Naira");
                int UserReply1 = Convert.ToInt32(Console.ReadLine());
                if (UserReply1 == 1)
                {
                    Console.WriteLine("Collect your cash\n");// CurrrencyNoteCount //ADD CoLOR
                    Console.WriteLine($"{UserReply / 1000} notes of 1000 Naira , amountin to {UserReply} \n");
                }
                if (UserReply1 == 2)
                {
                    Console.WriteLine("Collect your cash\n");// CurrrencyNoteCount 
                    Console.WriteLine($"There are {UserReply / 500} notes of 500 Naira , amountin to {UserReply } \n");
                }

            }
            else
            {
                Console.WriteLine("Insufficient funds");
            }


        }

        protected static void ChangeLanguage()
        {
            /*Console.WriteLine("Please Select a language");
            Console.WriteLine("2:  Pidgin");*/
            Authentication.PidginLogin();
        }

        void DisplayContinueOrExitOption()//interface function.....dis function soud be able to redirect you to loin or exit //redirection
        {
            Console.WriteLine("I want to \nPress 1: Continue\nPress 2: Register new user \nPress Any Other key: Exit");
            switch (Console.ReadLine())
            {
                case "1":
                    Authentication.Login();
                    break;
                case "2":
                    /*var Userdata = CollectData();
                    Authentication.Register(Userdata);
                    DisplayContinueOrExitOption();*/
                    break;
                /*case "3":
                    Authentication.();
                    DisplayContinueOrExitOption();
                    break;*/
                default:
                    Console.WriteLine("Thank you for Using this service");
                    break;
            };
        }

        protected void AtmMainMenu(string name, string password, SortedDictionary<int, Account> Database)
        {
            Console.WriteLine($"Hello {name} ,You are welcome! ");
            Console.WriteLine();
            Console.WriteLine("Press 1 : Check your balance \nPress 2 : Withdraw cash \nPress 3 : Change Language\nPress 4 : Exit\n ");
            switch (Console.ReadLine())
            {
                case "1":
                    CheckBalance(name, password, Database);
                    DisplayContinueOrExitOption();
                    break;
                case "2":
                    WithdrawCash(name, password, Database);
                    DisplayContinueOrExitOption();
                    break;
                case "3":
                    ChangeLanguage();
                    Console.WriteLine("Coming soon");
                    DisplayContinueOrExitOption();
                    break;
                case "4":
                    Console.WriteLine("Thank you for Using this service");
                    break;
                default:
                    {
                        DisplayContinueOrExitOption();
                        break;
                    }
            }
        }
    }
}
