﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLingualATM
{
    class Authentication: DefaultAtmInterface
    {
        static Account NewUser;
        static SortedDictionary<int, Account> Database = new SortedDictionary<int, Account>();
        static int UserCount = 0;
        public static void Register(Register model)
        {
            //var ValidatedPassword = passwordValidator(model.PassWord, model.ConfirmPassword);
            NewUser = new Account
            {
                FullName = $"{model.FirstName} {model.LastName}",
                setBvn = model.BVN,
                CVV = model.CVV,
                setPassword = model.Password,
                AccountBalance = model.AccountBalance,
            };

            Console.WriteLine($"Congratulations {NewUser.FullName}, your registration was successfull\n");
            Database.Add(UserCount++, NewUser);
            Console.WriteLine();
        }
        public static void Login()
        {
            Console.WriteLine("Please enter your firstname and lastname seperated by a space");
            var UserAcclaimedName = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(UserAcclaimedName))
            {
                Validator.ErrorMenu("Username");
            }
            Console.WriteLine("Please enter your Password ");
            var UserAcclaimedPassword = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(UserAcclaimedPassword))
            {
                Validator.ErrorMenu("Password");
            }

            UserAcclaimedName = UserAcclaimedName.Trim().ToLower();
            UserAcclaimedPassword = UserAcclaimedPassword.Trim().ToLower();
            var isValidated = validateLoginDetails(UserAcclaimedName, UserAcclaimedPassword);

            if (isValidated)
            {
                AtmMainMenu(UserAcclaimedName, UserAcclaimedPassword, Database);
            }
            else
            {

            }
        }

        public static void PidginLogin()
        {
            Console.WriteLine("Please enter your firstname and lastname seperated by a space");
            var UserAcclaimedName = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(UserAcclaimedName))
            {
                Validator.ErrorMenu("Username");
            }
            Console.WriteLine("Please enter your Password ");
            var UserAcclaimedPassword = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(UserAcclaimedPassword))
            {
                Validator.ErrorMenu("Password");
            }

            UserAcclaimedName = UserAcclaimedName.Trim().ToLower();
            UserAcclaimedPassword = UserAcclaimedPassword.Trim().ToLower();
            var isValidated = validateLoginDetails(UserAcclaimedName, UserAcclaimedPassword);

            if (isValidated)
            {
                AtmMainMenu(UserAcclaimedName, UserAcclaimedPassword, Database);
            }
            else
            {

            }
        }

        public static bool validateLoginDetails(string Username, string password)
        {
            var isValidated = false;
            var result = Database.Where(x => x.Value.FullName.Equals(Username)).Where(y => y.Value.setPassword.Equals(password));

            if (result.Count() == 0)
            {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Your username or password is Incorrect\nor you aren't registered to this service\n");
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Please run this program again to try again");
                Console.ForegroundColor = ConsoleColor.White;
            }
            else
            {
                isValidated = true;
            }
            return isValidated;
        }
    }
}
