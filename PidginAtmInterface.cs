﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLingualATM
{
    class PidginAtmInterface : ATM
    {
        public void Run()
        {
            UserstatusMenu();
        }

        public void UserstatusMenu()
        {
            Console.WriteLine("If you are a first time user of this service  ");
            Console.WriteLine("Press 1 to register\n");
            Console.WriteLine("If you are an existing user of this service ");
            Console.WriteLine("Press 2 to Login\n");
            var UserStatus = Console.ReadLine();
            if (UserStatus == "1")
            {
                var Userdata = CollectData(); //collectinfo will return formdata
                Authentication.Register(Userdata);
                DisplayContinueOrExitOption();
            }
            else
            {

            }
        }

        Register CollectData()
        {
            Random RandomNumber = new Random();
            Register FormData;
            var menu = new StringBuilder();

            int cvv;
            string Firstname;
            string Lastname;
            int? bvn;
            string password;

            menu.Append("Enter your details:");
            Console.WriteLine(menu.ToString());
            Console.WriteLine("Abeg, Type your firstname");
            Firstname = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(Firstname))
            {
                Validator.ErrorMenu("Firstname");
            }
            Console.WriteLine("Abeg, Type your Lastname");
            Lastname = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(Lastname))
            {
                Validator.ErrorMenu("Lastname");
            }
        enterCvv:
            Console.WriteLine("Abeg, Type your 3-digit cvv ");
            Console.WriteLine("---> The 3-digit number wey dey for the back of your ATM CARD");
            try
            {
                cvv = Convert.ToInt32(Console.ReadLine());
                if (cvv.ToString().Length == 0)
                {
                    Validator.ErrorMenu("CVV");
                }
                if (cvv.ToString().Length != 3)
                {
                    Console.WriteLine();
                    Console.WriteLine("Your cvv must to get ONLY Three(3) digits");
                    goto enterCvv;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                goto enterCvv;
            }
        EnterBvn:
            Console.WriteLine("Abeg, Type your 8-digit bvn");
            try
            {
                bvn = (int?)Convert.ToInt32(Console.ReadLine());
                if (bvn.ToString().Length != 8)
                {
                    Console.WriteLine();
                    Console.WriteLine("Your BVN must to get ONLY Eight(8) digits");
                    goto EnterBvn;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                goto EnterBvn;
            }
        EnterPassword:
            Console.WriteLine("Abeg, Type your pin");
            password = Console.ReadLine();
            if (String.IsNullOrWhiteSpace(password))
            {
                Validator.ErrorMenu("Pin");
            }
            if (password.Length < 4)
            {
                Console.WriteLine();
                Console.WriteLine("Your pin must to get up to four(4) digits");
                goto EnterPassword;
            }

            FormData = new Register
            {
                FirstName = Firstname,
                LastName = Lastname,
                BVN = bvn,
                CVV = cvv,
                Password = password,
                AccountBalance = (int)RandomNumber.Next(150000, 2000000),
            };

            return FormData;

        }

         void DisplayContinueOrExitOption()//interface function.....dis function soud be able to redirect you to loin or exit //redirection
        {
            Console.WriteLine("I want to \nPress 1: Continue\nPress 2: Register new user \nPress Any Other key: Exit");
            switch (Console.ReadLine())
            {
                case "1":
                    Authentication.Login();
                    break;
                case "2":
                    var Userdata = CollectData();
                    Authentication.Register(Userdata);
                    DisplayContinueOrExitOption();
                    break;
                /*case "3":
                    Authentication.();
                    DisplayContinueOrExitOption();
                    break;*/
                default:
                    Console.WriteLine("Thank you for Using this service");
                    break;
            };
        }

        protected void AtmMainMenu(string name, string password, SortedDictionary<int, Account> Database)
        {
            Console.WriteLine($"Hello {name} ,You are welcome! ");
            Console.WriteLine();
            Console.WriteLine("Press 1 : Check your balance \nPress 2 : Withdraw cash \nPress 3 : Change Language\nPress 4 : Exit\n ");
            switch (Console.ReadLine())
            {
                case "1":
                    CheckBalance(name, password, Database);
                    DisplayContinueOrExitOption();
                    break;
                case "2":
                    WithdrawCash(name, password, Database);
                    DisplayContinueOrExitOption();
                    break;
                case "3":
                    ChangeLanguage();
                    Console.WriteLine("Coming soon");
                    DisplayContinueOrExitOption();
                    break;
                case "4":
                    Console.WriteLine("Thank you for Using this service");
                    break;
                default:
                    {
                        DisplayContinueOrExitOption();
                        break;
                    }
            }
        }
    }
    }
