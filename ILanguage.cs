﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLingualATM
{
    interface ILanguage
    {
        void UserstatusMenu();
        Register CollectData();
        void DisplayContinueOrExitOption();
        void AtmMAinMenu();

    }
}
