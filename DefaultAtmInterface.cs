﻿using System;
using System.Text;
using System.Collections.Generic;

namespace MultiLingualATM
{
    class DefaultAtmInterface:ATM
    {
        public void Run()
        {
            UserstatusMenu();
        }

        public void UserstatusMenu()
        {
            Console.WriteLine("If you are a first time user of this service  ");
            Console.WriteLine("Press 1 to register\n");
            Console.WriteLine("If you are an existing user of this service ");
            Console.WriteLine("Press 2 to Login\n");
            var UserStatus = Console.ReadLine();
            if (UserStatus == "1")
            {
                var Userdata = CollectData(); //collectinfo will return formdata
                Authentication.Register(Userdata);
                DisplayContinueOrExitOption();
            }
            else
            {

            }
        }

       static Register CollectData()
        {
            Random RandomNumber = new Random();
            Register FormData;
            var menu = new StringBuilder();

            int cvv;
            string Firstname;
            string Lastname;
            int? bvn;
            string password;

            menu.Append("please enter your details");
            Console.WriteLine(menu.ToString());
            Console.WriteLine("Enter your firstname");
            Firstname = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(Firstname))
            {
                Validator.ErrorMenu("Firstname");
            }
            Console.WriteLine("Enter your Lastname");
            Lastname = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(Lastname))
            {
                Validator.ErrorMenu("Lastname");
            }
        enterCvv:
            Console.WriteLine("Enter your cvv");
            try
            {
                cvv = Convert.ToInt32(Console.ReadLine());
                if (cvv.ToString().Length == 0)
                {
                    //Validator.ErrorMenu("CVV");
                }
                if (cvv.ToString().Length != 3)
                {
                    Console.WriteLine();
                    Console.WriteLine("Your cvv must be contain ONLY 3 digits");
                    goto enterCvv;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                goto enterCvv;
            }
        EnterBvn:
            Console.WriteLine("Enter your bvn");
            try
            {
                bvn = (int?)Convert.ToInt32(Console.ReadLine());
                if (bvn.ToString().Length != 8)
                {
                    Console.WriteLine();
                    Console.WriteLine("Your BVN must contain Eight(8) digits");
                    goto EnterBvn;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                goto EnterBvn;
            }
        EnterPassword:
            Console.WriteLine("Enter your password");
            password = Console.ReadLine();
            if (String.IsNullOrWhiteSpace(password))
            {
                Validator.ErrorMenu("Password");
            }
            if (password.Length < 4)
            {
                Console.WriteLine();
                Console.WriteLine("Your password must contain at least four(4) characters");
                goto EnterPassword;
            }

            FormData = new Register
            {
                FirstName = Firstname,
                LastName = Lastname,
                BVN = bvn,
                CVV = cvv,
                Password = password,
                AccountBalance = (int)RandomNumber.Next(150000, 2000000),
            };

            return FormData;

        }

        static void DisplayContinueOrExitOption()//interface function.....dis function soud be able to redirect you to loin or exit //redirection
        {
            Console.WriteLine("I want to \nPress 1: Continue\nPress 2: Register new user \nPress Any Other key: Exit");
             switch (Console.ReadLine()) 
             {
                case "1":
                    Authentication.Login();
                    break;
                case "2":
                    var Userdata = CollectData(); 
                    Authentication.Register(Userdata);
                    DisplayContinueOrExitOption();
                    break;
                /*case "3":
                    Authentication.();
                    DisplayContinueOrExitOption();
                    break;*/
                default:
                    Console.WriteLine("Thank you for Using this service");
                    break; 
             };
        }

        protected static void AtmMainMenu(string name, string password, SortedDictionary<int, Account> Database)
        {           
            /*Console.WriteLine($"Hello {NewUser.FullName} ,You are welcome! ");*/
            Console.WriteLine();
            Console.WriteLine("Press 1 : Check your balance \nPress 2 : Withdraw cash \nPress 3 : Change Language\nPress 4 : Exit\n ");
            switch (Console.ReadLine())
            {
                case "1":
                    CheckBalance(name,password,Database);
                    DisplayContinueOrExitOption();
                    break;
                case "2":
                    WithdrawCash(name,password,Database);
                    DisplayContinueOrExitOption();
                    break;
                case "3":
                    ChangeLanguage();
                    Console.WriteLine("Coming soon");
                    DisplayContinueOrExitOption();
                    break;
                case "4":
                    Console.WriteLine("Thank you for Using this service");
                    break;
                default:
                    {
                        DisplayContinueOrExitOption();
                        break;
                    }
            }
        }

    }
}
